import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';
import 'package:math_game/Page/game/divide.dart';
import 'package:math_game/Page/game/minus.dart';
import 'package:math_game/Page/game/multiple.dart';
import 'package:math_game/Page/game/plus.dart';
import 'package:math_game/Page/menu/ChoosePage.dart';
import 'package:math_game/Page/menu/main_menu.dart';
import 'package:math_game/models/stateMinusController_model.dart';
import 'package:provider/provider.dart';
import 'models/stateDivideController_model.dart';
import 'models/stateMultipleController_model.dart';
import 'models/statePlusController_model.dart';

void main() {
  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider(
        create: (context) => StatePlusController(),
      ),
      ChangeNotifierProvider(
        create: (context) => StateMinusController(),
      ),
      ChangeNotifierProvider(
        create: (context) => StateMultipleController(),
      ),
      ChangeNotifierProvider(
        create: (context) => StateDivideController(),
      ),
    ],
    child: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        routes: {
          '/main_menu': (context) => MainMenu(),
          '/plus_game': (context) => PlusGame(),
          '/minus_game': (context) => MinusGame(),
          '/multiple_game': (context) => MultipleGame(),
          '/divide_game': (context) => DivideGame(),
          '/choose_page': (context) => ChoosePage(),
        },
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        title: 'Mathematics Game',
        theme: ThemeData(
          primarySwatch: myColor,
          fontFamily: 'Raleway',
        ),
        home: MainMenu(),
      ),
    );
  }
}

MaterialColor myColor = MaterialColor(
  0xFF0CC0DF,
  <int, Color>{
    50: Color(0xFFE5F8FA),
    100: Color(0xFF09A5C5),
    200: Color(0xFF09A5C5),
    300: Color(0xFF09A5C5),
    400: Color(0xFF26C6E8),
    500: Color(0xFF0CC0DF),
    600: Color(0xFF09A5C5),
    700: Color(0xFF0AAECE),
    800: Color(0xFF09A5C5),
    900: Color(0xFF0891B5),
  },
);

MaterialColor colorBlack = MaterialColor(
  0xFF000000,
  <int, Color>{
    50: Color(0xFF000000),
    100: Color(0xFF000000),
    200: Color(0xFF000000),
    300: Color(0xFF000000),
    400: Color(0xFF000000),
    500: Color(0xFF000000),
    600: Color(0xFF000000),
    700: Color(0xFF000000),
    800: Color(0xFF000000),
    900: Color(0xFF000000),
  },
);
