import 'package:audioplayers/audioplayers.dart';
import 'package:blurry/resources/arrays.dart';
import 'package:flutter/material.dart';
import 'package:linear_timer/linear_timer.dart';
import 'dart:math';
import 'package:blurry/blurry.dart';
import 'package:math_game/Component/button_plus.dart';
import 'package:math_game/Page/menu/main_menu.dart';
import 'package:provider/provider.dart';
import 'package:quickalert/quickalert.dart';
import '../../models/statePlusController_model.dart';
import '../menu/end.dart';

class PlusGame extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final statePlusController = Provider.of<StatePlusController>(context);
    statePlusController.initialGame();
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    final fontSize =
        screenWidth < screenHeight ? screenWidth * 0.2 : screenHeight * 0.2;
    return Container(
      decoration: const BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.fill,
          image: AssetImage('/image/BGBlue.png'),
        ),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          title: Text(
            'การบวก',
            style: TextStyle(
              color: Colors.white,
              fontSize: screenWidth < 1920 ? fontSize * 0.3 : fontSize * 0.15,
              fontWeight: FontWeight.w400,
            ),
          ),
          actions: <Widget>[
            Padding(
              padding: const EdgeInsets.only(right: 10),
              child: Center(
                  child: Text(
                "Score: ${statePlusController.score}",
                style: TextStyle(
                    fontSize:
                        screenWidth < 1920 ? fontSize * 0.22 : fontSize * 0.15,
                    color: Colors.white),
              )),
            )
          ],
        ),
        body: Stack(
          children: [
            LinearTimer(
              key: UniqueKey(),
              forward: false,
              color: Colors.white,
              backgroundColor: Colors.transparent,
              duration: Duration(milliseconds: statePlusController.time),
              onTimerEnd: () {
                if (statePlusController.score == 0) {
                  player.play('lose.mp3');
                  QuickAlert.show(
                    context: context,
                    type: QuickAlertType.error,
                    barrierDismissible: false,
                    animType: QuickAlertAnimType.slideInDown,
                    title: 'ว้าาา!! คุณทำไม่ได้เลย',
                    text: 'คุณอยู่ในเกณฑ์: เด็กอ่อน!!!',
                    barrierColor: Colors.white.withOpacity(0.7),
                    titleColor: Colors.black,
                    textColor: Colors.black,
                    confirmBtnText: "เล่นอีกครั้ง",
                    showCancelBtn: true,
                    onConfirmBtnTap: () {
                      player.play('click_cute.mp3');
                      statePlusController.resetGame();
                      Navigator.pop(context);
                    },
                    cancelBtnText: "กลับไปหน้าหลัก",
                    onCancelBtnTap: () {
                      statePlusController.resetGame();
                      player.play('click_cute.mp3');
                      Navigator.pushNamedAndRemoveUntil(
                        context,
                        '/main_menu',
                        ModalRoute.withName('/'),
                      );
                    },
                  );
                }
                if (statePlusController.score > 0 &&
                    statePlusController.score <= 10) {
                  player.play('warnning.mp3');
                  QuickAlert.show(
                    context: context,
                    type: QuickAlertType.warning,
                    barrierDismissible: false,
                    animType: QuickAlertAnimType.slideInDown,
                    title:
                        'หืมม!! คุณทำได้: ${statePlusController.score} คะแนน',
                    text: 'คุณอยู่ในเกณฑ์: เด็กฝึกหัด!!!',
                    barrierColor: Colors.white.withOpacity(0.7),
                    titleColor: Colors.black,
                    textColor: Colors.black,
                    confirmBtnText: "เล่นอีกครั้ง",
                    showCancelBtn: true,
                    onConfirmBtnTap: () {
                      statePlusController.resetGame();
                      player.play('click_cute.mp3');
                      Navigator.pop(context);
                    },
                    cancelBtnText: "กลับไปหน้าหลัก",
                    onCancelBtnTap: () {
                      statePlusController.resetGame();
                      player.play('click_cute.mp3');
                      Navigator.pushNamedAndRemoveUntil(
                        context,
                        '/main_menu',
                        (Route<dynamic> route) => false,
                      );
                    },
                  );
                }
                if (statePlusController.score >= 11 &&
                    statePlusController.score <= 40) {
                  player.play('basic.mp3');
                  QuickAlert.show(
                    context: context,
                    barrierDismissible: false,
                    type: QuickAlertType.success,
                    animType: QuickAlertAnimType.slideInDown,
                    title:
                        'โอ้วว!! คุณทำได้: ${statePlusController.score} คะแนน',
                    text: 'คุณอยู่ในเกณฑ์: เด็กทั่วไป',
                    barrierColor: Colors.white.withOpacity(0.7),
                    titleColor: Colors.black,
                    textColor: Colors.black,
                    confirmBtnText: "เล่นอีกครั้ง",
                    showCancelBtn: true,
                    onConfirmBtnTap: () {
                      statePlusController.resetGame();
                      player.play('click_cute.mp3');
                      Navigator.pop(context);
                    },
                    cancelBtnText: "กลับไปหน้าหลัก",
                    onCancelBtnTap: () {
                      statePlusController.resetGame();
                      player.play('click_cute.mp3');
                      Navigator.pushNamedAndRemoveUntil(
                        context,
                        '/main_menu',
                        (Route<dynamic> route) => false,
                      );
                    },
                  );
                }
                if (statePlusController.score >= 41) {
                  player.play('win.mp3');
                  QuickAlert.show(
                    context: context,
                    barrierDismissible: false,
                    type: QuickAlertType.info,
                    animType: QuickAlertAnimType.slideInDown,
                    title:
                        'โอ้วว!! คุณทำได้: ${statePlusController.score} คะแนน',
                    text: 'คุณอยู่ในเกณฑ์: เด็กเทพ',
                    barrierColor: Colors.white.withOpacity(0.7),
                    titleColor: Colors.black,
                    textColor: Colors.black,
                    confirmBtnText: "เล่นอีกครั้ง",
                    showCancelBtn: true,
                    onConfirmBtnTap: () {
                      statePlusController.resetGame();
                      player.play('click_cute.mp3');
                      Navigator.pop(context);
                    },
                    cancelBtnText: "กลับไปหน้าหลัก",
                    onCancelBtnTap: () {
                      statePlusController.resetGame();
                      player.play('click_cute.mp3');
                      Navigator.pushNamedAndRemoveUntil(
                        context,
                        '/main_menu',
                        (Route<dynamic> route) => false,
                      );
                    },
                  );
                }
              },
            ),
            Padding(
              padding: EdgeInsets.only(top: screenHeight * 0.1),
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.only(bottom: screenHeight * 0.1),
                    child: Center(
                      child: Padding(
                        padding: EdgeInsets.all(screenWidth * 0.02),
                        child: Stack(
                          alignment: Alignment.center,
                          children: [
                            Container(
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius:
                                    BorderRadius.circular(fontSize / 2),
                              ),
                              height: fontSize,
                              width: fontSize * 5,
                            ),
                            Text(
                              "${statePlusController.equation}",
                              style: TextStyle(
                                  fontSize: fontSize * 0.5, color: Colors.cyan),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Center(
                      child: Column(
                        children: [
                          Expanded(
                            flex: 1,
                            child: Row(
                              children: [
                                Expanded(
                                    child: ButtonPlusSelect(
                                  selected: statePlusController.listSelect[0],
                                  screenHeight: screenHeight,
                                  screenWidth: screenWidth,
                                  result: statePlusController.result,
                                  fontSize: fontSize,
                                )),
                                Expanded(
                                    child: ButtonPlusSelect(
                                  selected: statePlusController.listSelect[1],
                                  screenHeight: screenHeight,
                                  screenWidth: screenWidth,
                                  result: statePlusController.result,
                                  fontSize: fontSize,
                                )),
                              ],
                            ),
                          ),
                          Expanded(
                            child: Row(
                              children: [
                                Expanded(
                                    child: ButtonPlusSelect(
                                  selected: statePlusController.listSelect[2],
                                  screenHeight: screenHeight,
                                  screenWidth: screenWidth,
                                  result: statePlusController.result,
                                  fontSize: fontSize,
                                )),
                                Expanded(
                                    child: ButtonPlusSelect(
                                  selected: statePlusController.listSelect[3],
                                  screenHeight: screenHeight,
                                  screenWidth: screenWidth,
                                  result: statePlusController.result,
                                  fontSize: fontSize,
                                )),
                              ],
                            ),
                          ),
                          screenWidth <= 500
                              ? Expanded(child: SizedBox())
                              : Container(
                                  height: 0,
                                )
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

var player = AudioCache();

Color randomColor() {
  final random = Random();
  final hue = random.nextInt(360);
  final saturation = 0.2 + random.nextDouble() * 0.8;
  final value = 0.2 + random.nextDouble() * 0.8;
  final hsv = HSVColor.fromAHSV(1.0, hue.toDouble(), saturation, value);
  return hsv.toColor();
}
