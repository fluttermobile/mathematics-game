import 'package:audioplayers/audioplayers.dart';
import 'package:floating_text/floating_text.dart';
import 'package:flutter/material.dart';
import 'package:widget_and_text_animator/widget_and_text_animator.dart';

import 'ChoosePage.dart';

class MainMenu extends StatefulWidget {
  @override
  State<MainMenu> createState() => _MainMenuState();
}

var player = AudioCache(respectSilence: true);

class _MainMenuState extends State<MainMenu> {
  // AudioCache audioCache = AudioCache();
  // late AudioPlayer fixedPlayer;

  // @override
  // void initState() {
  //   super.initState();
  //   fixedPlayer = AudioPlayer();
  //   playBackgroundMusic();
  // }

  // void playBackgroundMusic() async {
  //   await audioCache.load('soundBG.mp3'); // โหลดไฟล์เสียงไว้ก่อน
  //   fixedPlayer = await audioCache.loop('soundBG.mp3');
  //   fixedPlayer.setReleaseMode(ReleaseMode.LOOP);
  // }

  // @override
  // void dispose() {
  //   fixedPlayer.stop();
  //   fixedPlayer.dispose();
  //   super.dispose();
  // }

  // void playBackgroundMusic() async {
  //   int result = await audioPlayer.play('soundBG.mp3');
  //   if (result == 1) {
  //     print("Audio playing");
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    double fontSize = 0.0;
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.fill,
          image: screenWidth < 600
              ? AssetImage('/image/bg1.png')
              : AssetImage('/image/BGnon1.png'),
        ),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
            screenWidth = MediaQuery.of(context).size.width;
            screenHeight = MediaQuery.of(context).size.height;
            fontSize = screenWidth < screenHeight
                ? screenWidth * 0.2
                : screenHeight * 0.2;
            return Center(
              child: Padding(
                padding: EdgeInsets.only(top: screenHeight * 0.1),
                child: Column(
                  children: <Widget>[
                    Stack(
                      children: [
                        FloatingText(
                          text: "MATH THINK FAST",
                          repeat: true,
                          duration: Duration(milliseconds: 200),
                          floatingTextStyle: TextStyle(
                            color: Colors.blue,
                            fontSize: screenWidth < 1920
                                ? fontSize * 0.7
                                : fontSize * 0.1,
                            fontWeight: FontWeight.bold,
                          ),
                          textStyle: TextStyle(
                            color: Colors.black,
                            fontSize: fontSize * 0.5,
                            fontFamily: 'RobotoMono',
                            fontWeight: FontWeight.bold,
                          ),
                        )
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: screenHeight * 0.1),
                      child: ConstrainedBox(
                        constraints: screenWidth < 600
                            ? BoxConstraints(maxHeight: screenHeight * 0.3)
                            : BoxConstraints(maxHeight: screenHeight * 0.28),
                        child: AspectRatio(
                          aspectRatio: 16 / 9,
                          child: WidgetAnimator(
                            atRestEffect: WidgetRestingEffects.swing(),
                            child: Container(
                              child: Image.asset("./image/cal.png"),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          top: screenHeight * 0.1,
                          right: screenWidth * 0.2,
                          left: screenWidth * 0.2),
                      child: Container(
                          height: screenWidth < 600
                              ? screenHeight * 0.15
                              : screenHeight * 0.22,
                          width: screenWidth < 600
                              ? screenWidth * 0.6
                              : screenWidth * 0.4,
                          padding: const EdgeInsets.all(20),
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              primary: Color.fromARGB(255, 244, 184, 91),
                              side: BorderSide(width: 3, color: Colors.brown),
                              elevation: 3,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(
                                30,
                              )),
                            ),
                            child: Text(
                              'PLAY',
                              style: TextStyle(
                                color: Color.fromARGB(255, 71, 32, 0),
                                fontWeight: FontWeight.bold,
                                fontSize: fontSize * 0.5,
                              ),
                            ),
                            onPressed: () {
                              final player = AudioCache();
                              player.play('click_cute.mp3');

                              Navigator.pushReplacementNamed(
                                context,
                                '/choose_page',
                              );
                            },
                          )),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
