import 'package:flutter/material.dart';
import 'package:math_game/Page/game/plus.dart';

class EndGame extends StatefulWidget {
  const EndGame({super.key});

  @override
  State<EndGame> createState() => _EndGameState();
}

class _EndGameState extends State<EndGame> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.pink.shade100,
      appBar: AppBar(
        backgroundColor: Colors.pinkAccent[100],
      ),
      body: Center(
        child: ListView(
          children: <Widget>[
            Container(
              height: 40,
            ),
            Container(
                child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "คณิตคิดเลขเร็ว",
                      style: TextStyle(
                          fontSize: 45,
                          color: Color.fromARGB(255, 0, 0, 0),
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ],
            )),
            Container(
              height: 120,
            ),
            IconButton(
              splashRadius: 120,
              icon: Image.asset('/image/reButton.png'),
              iconSize: 200,
              onPressed: () {
                Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(builder: (context) => PlusGame()),
                );
              },
            ),
            Container(
              height: 130,
            ),
            Container(
                height: 100,
                padding: const EdgeInsets.all(20),
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: Color.fromARGB(255, 255, 132, 126),
                    side: BorderSide(width: 3, color: Colors.brown),
                    elevation: 3,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(
                      30,
                    )),
                  ),
                  child: const Text(
                    ' Exit ',
                    style: TextStyle(fontSize: 30, color: Colors.black),
                  ),
                  onPressed: () {},
                )),
          ],
        ),
      ),
    );
  }
}
