import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:math_game/Page/game/divide.dart';
import 'package:math_game/Page/game/minus.dart';
import 'package:math_game/Page/game/multiple.dart';
import 'package:math_game/Page/game/plus.dart';
import 'package:floating_text/floating_text.dart';
import 'package:flutter_animated_button/flutter_animated_button.dart';

import 'main_menu.dart';

class ChoosePage extends StatefulWidget {
  const ChoosePage({super.key});

  @override
  State<ChoosePage> createState() => _ChoosePageState();
}

var player = AudioCache();

class _ChoosePageState extends State<ChoosePage> {
  // AudioCache audioCache = AudioCache();
  // late AudioPlayer fixedPlayer;

  // @override
  // void initState() {
  //   super.initState();
  //   fixedPlayer = AudioPlayer();
  //   playBackgroundMusic();
  // }

  // void playBackgroundMusic() async {
  //   fixedPlayer = await audioCache.loop('BGChicken.mp3');
  //   fixedPlayer.setReleaseMode(ReleaseMode.LOOP);
  // }

  // @override
  // void dispose() {
  //   fixedPlayer.stop();
  //   fixedPlayer.dispose();
  //   super.dispose();
  // }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    var submitTextStyle = TextStyle(
        fontSize: 28,
        letterSpacing: 5,
        color: Colors.white,
        fontWeight: FontWeight.w300);
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.fill,
          image: screenWidth < 600
              ? AssetImage('/image/bg2.png')
              : AssetImage('/image/BGnon2.png'),
        ),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: ListView(
          children: [
            Container(
                child: Column(
              children: [
                Container(
                  height: 70,
                ),
                Container(
                    width: 250,
                    height: 70,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            FloatingText(
                              text: "หมวดหมู่",
                              repeat: true,
                              duration: Duration(milliseconds: 200),
                              floatingTextStyle: TextStyle(
                                color: Colors.red,
                                fontSize: 45,
                                fontWeight: FontWeight.bold,
                              ),
                              textStyle: TextStyle(
                                color: Color.fromARGB(255, 139, 67, 56),
                                fontSize: 35,
                                fontFamily: 'RobotoMono',
                                fontWeight: FontWeight.bold,
                              ),
                            )
                            // const Text(
                            //   "หมวดหมู่",
                            //   style: TextStyle(
                            //       fontSize: 41,
                            //       color: Color.fromARGB(255, 139, 67, 56),
                            //       fontWeight: FontWeight.bold),
                            // ),
                          ],
                        ),
                      ],
                    )),
                Container(
                  height: 30,
                ),
                Container(
                    height: 100,
                    width: 200,
                    padding: const EdgeInsets.all(20),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Color.fromARGB(255, 255, 132, 126),
                        side: BorderSide(width: 3, color: Colors.brown),
                        elevation: 3,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(
                          30,
                        )),
                      ),
                      child: const Text(
                        '+',
                        style: TextStyle(
                          color: Color.fromARGB(255, 255, 255, 255),
                          fontWeight: FontWeight.bold,
                          fontSize: 50,
                        ),
                      ),
                      onPressed: () {
                        player = AudioCache();
                        player.play('click_cute.mp3');
                        Navigator.pushReplacementNamed(
                          context,
                          '/plus_game',
                        );
                      },
                    )),
                Container(
                  height: 25,
                ),
                Container(
                    height: 100,
                    width: 200,
                    padding: const EdgeInsets.all(20),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Color.fromARGB(255, 255, 132, 126),
                        side: BorderSide(width: 3, color: Colors.brown),
                        elevation: 3,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(
                          30,
                        )),
                      ),
                      child: const Text(
                        '-',
                        style: TextStyle(
                          color: Color.fromARGB(255, 255, 255, 255),
                          fontWeight: FontWeight.bold,
                          fontSize: 60,
                        ),
                      ),
                      onPressed: () {
                        player.play('click_cute.mp3');
                        Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(builder: (context) => MinusGame()),
                        );
                      },
                    )),
                Container(
                  height: 25,
                ),
                Container(
                    height: 100,
                    width: 200,
                    padding: const EdgeInsets.all(20),
                    // child: AnimatedButton(
                    //   height: 70,
                    //   width: 200,
                    //   text: 'SUBMIT',
                    //   isReverse: true,
                    //   selectedTextColor: Colors.black,
                    //   transitionType: TransitionType.LEFT_TO_RIGHT,
                    //   textStyle: submitTextStyle,
                    //   backgroundColor: Colors.black,
                    //   borderColor: Colors.white,
                    //   borderRadius: 50,
                    //   borderWidth: 2,
                    //   animationDuration: const Duration(milliseconds: 500),

                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Color.fromARGB(255, 255, 132, 126),
                        side: BorderSide(width: 3, color: Colors.brown),
                        elevation: 3,
                        shape: RoundedRectangleBorder(
                            //to set border radius to button
                            borderRadius: BorderRadius.circular(
                          30,
                        )),
                      ),
                      child: const Text(
                        'x',
                        style: TextStyle(
                          color: Color.fromARGB(255, 255, 255, 255),
                          fontWeight: FontWeight.bold,
                          fontSize: 50,
                        ),
                      ),
                      onPressed: () {
                        player.play('click_cute.mp3');
                        Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                              builder: (context) => MultipleGame()),
                        );
                      },
                    )),
                Container(
                  height: 25,
                ),
                Container(
                    height: 100,
                    width: 200,
                    padding: const EdgeInsets.all(20),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Color.fromARGB(255, 255, 132, 126),
                        side: BorderSide(width: 3, color: Colors.brown),
                        elevation: 3,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(
                          30,
                        )),
                      ),
                      child: const Text(
                        '÷',
                        style: TextStyle(
                          color: Color.fromARGB(255, 255, 255, 255),
                          fontWeight: FontWeight.bold,
                          fontSize: 50,
                        ),
                      ),
                      onPressed: () {
                        player.play('click_cute.mp3');
                        Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(builder: (context) => DivideGame()),
                        );
                      },
                    )),
                Container(
                  height: 25,
                ),
                Container(
                    height: 100,
                    width: 200,
                    padding: const EdgeInsets.all(20),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Color.fromARGB(255, 255, 132, 126),
                        side: BorderSide(width: 3, color: Colors.brown),
                        elevation: 3,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(
                          30,
                        )),
                      ),
                      child: const Text(
                        'ออก',
                        style: TextStyle(
                          color: Color.fromARGB(255, 255, 255, 255),
                          fontWeight: FontWeight.bold,
                          fontSize: 40,
                        ),
                      ),
                      onPressed: () {
                        player.play('click_cute.mp3');
                        Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(builder: (context) => MainMenu()),
                        );
                      },
                    )),
                Container(
                  height: 25,
                ),
              ],
            )),
            Container(
              height: 20,
            ),
          ],
        ),
      ),
    );
  }
}
