import 'dart:math';

import 'package:flutter/widgets.dart';

/// using the mixin concept of dart that we have discussed
/// in our previous chapter
class StateDivideController with ChangeNotifier {
  int value1 = 0;
  int value2 = 0;
  int result = 0;
  String equation = "";
  List listSelect = [];
  int score = 0;
  int time = 10000;

  void increaseValue() {
    score++;
    print("Score: ${score}");
    notifyListeners();
  }

  void initialGame() {
    randomNumberSimple();
    equation = value1 > value2
        ? "${value1} ÷ ${value2} = ?"
        : "${value2} ÷ ${value1} = ?";
    listSelect = [result, result + 1, result + 2, result + 3];
    listSelect.shuffle(Random());
    print("genarated Equation");
    // notifyListeners();
  }

  void genarateEquation() {
    randomNumberSimple();
    equation = value1 > value2
        ? "${value1} ÷ ${value2} = ?"
        : "${value2} ÷ ${value1} = ?";
    listSelect.clear();
    listSelect = [result, result + 1, result + 2, result + 3];
    listSelect.shuffle(Random());
    print("genarated Equation");
    notifyListeners();
  }

  void randomNumberSimple() {
    // สุ่ม value1 และ value2 โดยใช้ฟังก์ชัน generateRandomNumbers
    generateRandomNumbers(12, (num1, num2) {
      value1 = num1;
      value2 = num2;
    });

    result = value1 ~/ value2; // หารและปัดเศษ
  }

  void generateRandomNumbers(int maxNumber, Function(int, int) callback) {
    var random = Random();
    int value1 = random.nextInt(maxNumber) + 1;
    int value2 = random.nextInt(maxNumber) + 1;

    while (value2 == 0 || value1 % value2 != 0) {
      value1 = random.nextInt(maxNumber) + 1;
      value2 = random.nextInt(maxNumber) + 1;
    }

    callback(value1, value2); // ส่งผลลัพธ์กลับไปให้ฟังก์ชัน callback
  }

  void increase() {
    score += 1;
    if (time > 2000) {
      time -= 200;
    }
    notifyListeners();
    print("Time: ${time}");
    print("Score: ${score}");
  }

  void lose() {
    time = 1;
    notifyListeners();
  }

  void resetGame() {
    randomNumberSimple();
    equation = value1 > value2
        ? "${value1} ÷ ${value2} = ?"
        : "${value2} ÷ ${value1} = ?";
    listSelect.clear();
    listSelect = [result, result + 1, result + 2, result + 3];
    listSelect.shuffle(Random());
    score = 0;
    time = 10000;
    notifyListeners();
    print("reset");
  }
}
