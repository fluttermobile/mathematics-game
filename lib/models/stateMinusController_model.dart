import 'dart:math';

import 'package:flutter/widgets.dart';

/// using the mixin concept of dart that we have discussed
/// in our previous chapter
class StateMinusController with ChangeNotifier {
  int value1 = 0;
  int value2 = 0;
  int result = 0;
  String equation = "";
  List listSelect = [];
  int score = 0;
  int time = 10000;

  void initialGame() {
    value1 = Random().nextInt(10);
    value2 = Random().nextInt(10);
    result = value1 > value2 ? value1 - value2 : value2 - value1;
    equation = value1 > value2
        ? "${value1} - ${value2} = ?"
        : "${value2} - ${value1} = ?";
    listSelect = [result, result + 1, result + 2, result + 3];
    listSelect.shuffle(Random());
    // print("initial");
    // notifyListeners();
  }

  void genarateEquation() {
    value1 = Random().nextInt(10);
    value2 = Random().nextInt(10);
    result = value1 > value2 ? value1 - value2 : value2 - value1;
    equation = value1 > value2
        ? "${value1} - ${value2} = ?"
        : "${value2} - ${value1} = ?";
    listSelect.clear();
    listSelect = [result, result + 1, result + 2, result + 3];
    listSelect.shuffle(Random());
    print("genarated Equation");
    notifyListeners();
  }

  void increase() {
    score += 1;
    if (time > 2000) {
      time -= 200;
    }
    notifyListeners();
    print("Time: ${time}");
    print("Score: ${score}");
  }

  void lose() {
    time = 1;
    notifyListeners();
  }

  void resetGame() {
    value1 = Random().nextInt(10);
    value2 = Random().nextInt(10);
    result = value1 > value2 ? value1 - value2 : value2 - value1;
    equation = value1 > value2
        ? "${value1} - ${value2} = ?"
        : "${value2} - ${value1} = ?";
    listSelect = [result, result + 1, result + 2, result + 3];
    listSelect.shuffle(Random());
    time = 10000;
    score = 0;
    notifyListeners();
    print("reset");
    print("genarated Equation");
  }
}
