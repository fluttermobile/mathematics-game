import 'dart:math';

import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../models/stateDivideController_model.dart';
import '../models/stateMultipleController_model.dart';

class ButtonDivideSelect extends StatelessWidget {
  final int selected;
  final double screenHeight;
  final double screenWidth;
  final double fontSize;
  final int result;

  const ButtonDivideSelect({
    Key? key,
    required this.selected,
    required this.screenHeight,
    required this.screenWidth,
    required this.result,
    required this.fontSize,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final stateDivideController = Provider.of<StateDivideController>(context);
    return Padding(
      padding: EdgeInsets.all(screenHeight * 0.01),
      child: RawMaterialButton(
        onPressed: () {
          final player = AudioCache();
          if (selected == result) {
            player.play('correct.mp3');
            stateDivideController.increase();
            stateDivideController.genarateEquation();
          } else {
            player.play('incorrect.mp3');
            stateDivideController.lose();
          }
        },
        elevation: 2.0,
        fillColor: Colors.white,
        constraints: BoxConstraints.tight(
            Size(screenWidth * 0.15, screenHeight * 0.15)), // กำหนดขนาดของปุ่ม
        shape: screenWidth < 500
            ? CircleBorder()
            : RoundedRectangleBorder(), // กำหนดรูปร่างของปุ่มเป็นวงกลม
        child: Text(
          '${selected}',
          style: TextStyle(
            fontSize: fontSize * 0.5,
            color: Colors.cyan,
          ),
        ),
      ),
    );
  }
}

Color randomColor() {
  final random = Random();
  final hue = random.nextInt(360);
  final saturation = 0.2 + random.nextDouble() * 0.8;
  final value = 0.2 + random.nextDouble() * 0.8;
  final hsv = HSVColor.fromAHSV(1.0, hue.toDouble(), saturation, value);
  return hsv.toColor();
}
